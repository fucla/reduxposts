import React, { Component, PropTypes } from 'react';
import { reduxForm } from 'redux-form';
import { createPost } from '../actions/index';
import { Link } from 'react-router';

class PostsNew extends Component { // eslint-disable-line
  /**
   * context is implicitly passed from react router from parent. But in
   * general, avoid using context unless used with react router. API is
   * still changing.
   *
   * React will search all of this component's parents for a property called
   * router.
   */
   // eslint-disable
  static contextTypes = {
    router: PropTypes.object
  };

  onSubmit(props) {
    this.props.createPost(props)
      .then(() => {
        /* Blog post has been created, navigate the user to index. We navigate
         * by calling this.context.router.push with the new path to navigate to.
         */
         this.context.router.push('/');
      });
  }

  render() {
    // comes from redux form
    const { fields: { title, categories, content }, handleSubmit } = this.props;
    // const handleSubmit = this.props.handleSubmit;
    // const title = this.props.fields.title;

    return (
      <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
        <h3>Create a New Post</h3>
        <div className={
          `form-group ${title.touched && title.invalid ? 'has-danger' : ''}`}
        >
          <label>Title</label>
          { /* all title properties go into the input */ }
          <input type="text" className="form-control" { ...title } />
          <div className="text-help">
            { title.touched ? title.error : '' }
          </div>
        </div>
        <div className={
          `form-group ${categories.touched && categories.invalid ? 'has-danger' : ''}`}
        >
          <label>Categories</label>
          <input type="text" className="form-control" { ...categories } />
            <div className="text-help">
              { categories.touched ? categories.error : '' }
            </div>
        </div>
        <div className={
          `form-group ${content.touched && content.invalid ? 'has-danger' : ''}`}
        >
          <label>Content</label>
          <textarea className="form-control" { ...content } />
            <div className="text-help">
              { content.touched ? content.error : '' }
            </div>
        </div>

        <button type="submit" className="btn btn-primary">Submit</button>
        <Link to="/" className="btn btn-danger">Cancel</Link>
      </form>
    );
  }
}

PostsNew.propTypes = {
  handleSubmit: React.PropTypes.func,
  createPost: React.PropTypes.func,
  fields: React.PropTypes.shape({
    title: React.PropTypes.object,
    categories: React.PropTypes.object,
    content: React.PropTypes.object,
  }),
};

function validate(values) {
  const errors = {};

  if (!values.title) {
    errors.title = 'Enter a title';
  }
  if (!values.categories) {
    errors.categories = 'Enter categories';
  }
  if (!values.content) {
    errors.content = 'Enter some content';
  }

  return errors;
}

/**
 * connect, first argument is mapStateToProps, second is mapDispatchToProps.
 * reduxForm, first is form config, second is mapStateToProps, third is
 * mapDispatchToProps.
 */
export default reduxForm({
  form: 'PostsNewForm',
  fields: ['title', 'categories', 'content'],
  validate,
}, null, { createPost })(PostsNew);
