/* eslint-disable */
import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './components/app';
import PostsIndex from './components/posts_index';
import PostsNew from './components/posts_new';
import PostsShow from './components/posts_show';

// const Greeting = () => {
//   return <div>Hey there!</div>;
// };

/**
 * Forward slash is the root URL. Child path needs to be placed in parent
 * component. Accessed as this.props.children. IndexRoute passes a default
 * child component.
 *
 * Route param :id is passed as this.props.params.id
 */
export default (
  <Route path="/" component={ App }>
    <IndexRoute component={ PostsIndex } />
    <Route path="posts/new" component={ PostsNew } />
    <Route path="posts/:id" component={ PostsShow } />
  </Route>
);
